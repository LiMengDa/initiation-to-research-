\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[english]{babel}

\usepackage{hyperref}

\begin{document}
\title{Study on the paper \textit {CompCertS} }
\author{A.~Bardes, C.~Hopper, M.~Li }
\maketitle


\tableofcontents



\section{Background information}

The paper we study is \textit{\href{http://people.rennes.inria.fr/Frederic.Besson/compcerts.pdf}{CompCertS: A Memory-Aware Verified C Compiler using Pointer as Integer Semantics}} \cite{paper} by Frédéric Besson \cite{besson}, Sandrine Blazy \cite{blazy}, and Pierre Wilke \cite{wilke}. They propose a refinement of a verified C compiler known as CompCert. This compiler is meant for mission-critical system components. It uses verification to ensure that safe software is seamless compiled into a safe executable.

The new compiler, known as CompCertS, preserves memory consumption and verifies operations on pointers by casting them as integers.  \cite{compcert-C}. 


\subsection{Conferences}

This article appears in \textit{ITP 2017} - 8th International Conference on Interactive Theorem Proving, Sep 2017, Brasilia, Brazil. Springer, LNCS, 10499, pp.81-97, ITP 2017: Interactive Theorem Proving. 

\textbf{Interactive Theorem Proving (ITP)} \cite{ITP} is an annual international academic conference on the topic of automated theorem proving, proof assistants and related topics, ranging from theoretical foundations to implementation aspects and applications in program verification, security, and formalization of mathematics.
Together with \href{http://www.cadeinc.org/}{CADE} \cite{CADE}
and \href{http://www.tableaux-ar.org/}{TABLEAUX} \cite{TABLEAUX}
, ITP is usually one of the three main conferences of the International Joint Conference on Automated Reasoning (IJCAR) \cite{IJCAR}. The inaugural meeting of ITP was held on 11–14 July 2010 in Edinburgh, Scotland, as part of the Federated Logic Conference. It is the extension of the \textbf{Theorem Proving in Higher Order Logics (TPHOLs)} conference series to the broad field of interactive theorem proving. TPHOLs meetings took place every year from 1988 until 2009. 

\textbf{The Conference on Automated Deduction (CADE)} is the premier academic conference on automated deduction and related fields. The first CADE was organized in 1974 at the Argonne National Laboratory near Chicago. 

\textbf{The International Conference on Automated Reasoning with Analytic Tableaux and Related Methods (TABLEAUX)} is an annual international academic conference that deals with all aspects of automated reasoning with analytic tableaux. The first table convened in 1992. 

\textbf{The International Joint Conference on Automated Reasoning (IJCAR)} is a series of conferences on the topics of automated reasoning, automated deduction, and related fields. It is organized semi-regularly as a merger of other meetings. IJCAR replaces those independent conferences in the years it takes place. The conference is organized by CADE Inc., and CADE has always been one of the conferences partaking in IJCAR. IJCAR is the premier international joint conference on all aspects of automated reasoning, including foundations, implementations, and applications, comprising several leading conferences and workshops.

\subsection{Authors}

\href{http://people.rennes.inria.fr/Frederic.Besson/}{F. Besson}  is a researcher from Inria Rennes. He has also contributed works in security and network information management.
\href{http://people.irisa.fr/Sandrine.Blazy/}{S. Blazy} is professor of Computer Science from University of Rennes 1. \href{http://www.cs.yale.edu/homes/wilke-pierre/}{P. Wilke} is a postdoctoral fellow of Yale University. 

F. Besson and S. Blazy are in the \textbf{\href{https://team.inria.fr/celtique/}{Celtique}} team of the Inria Rennes - Bretagne Atlantique research centre \cite{celtique},  P. Wilke was previously a Phd student in this team. The overall goal of the Celtique project is to improve the security and reliability of software with semantics-based modeling, analysis and certification techniques. To achieve this goal, the project conducts work on improving semantic description and analysis techniques, as well as work on using proof assistants (most notably Coq) to develop and prove properties of these techniques.

Pierre Wilke now works with the Yale FLINT group for verifying large-scale systems software. They aim to build software structures, semantics, languages, environments, and automaton facilities. He is also a part of CertiKOF, which aims to construct novel verified operating systems.
 
 
\subsection{Previous Work}

\subsubsection{C Semantics}
The first formal C semantics were designed by Michael Norrish in \textit{C formalised in HOL} in 1998 as a revision of his PhD dissertation. In 2016, Memarian et al. provided a comprehensive overview of consensus about C semantics among community experts. More specifically, semantics for integer-pointer casts were originally formalized by Kang et al., in \textit{A formal C memory model supporting integer-pointer casts}. The approach for handling points in the current paper largely follows from Kang et al.

\subsubsection{CompCert}
CompCert is a formally verified C compiler written with the Coq proof assistant, introduced by X. Leroy \cite{x_leroy_compcert}. it provides formal guaranties that the behaviour of the compiled code is exactly the expected behaviour of the original source code. To do so, original and target languages have to be defined with formal semantics. However, CompCert has some drawbacks regarding the memory management. Firstly, memory allocation is always possible in CompCert. Therefore, there is no guarantee regarding the memory consumption. In particular, it could lead to a stack overflow error. Secondly, CompCert’s memory model limits pointer arithmetic. That means that every pointer operation results in an undefined behaviour of the memory model.

\subsubsection{A new memory model}
Previous work have been done by F.Besson, S. Blazy and P. Wilke on the development of a new memory model addressing these problems. In a first time, symbolic values have been introduced in \cite{besson_blazy_wilke_2014} leading to the new model. Then this new model have been formally proved in  \cite{besson_blazy_wilke_2015}. Last year the same authors produced another augmentation of CompCert that adds semantics for bitwise pointer arithmetic and access to uninitialized data. \cite{besson_blazy_wilke_2017}

\section{Solutions provided by CompCertS}

CompCertS (for CompCert with Symbolic values) address the two problems identified earlier. Henceforth, all passes are using the new memory model developed in last year previous works \cite{besson_blazy_wilke_2014, besson_blazy_wilke_2015}. Therefore CompCertS provides strong guarantees about the relative memory us-
age of the source and target program. CompCertS is definitively safer than CompCert.

\section{Related Projects and industrial usage}
CompCert is in an industrial partnership with AbsInt Angewandte Informatik GmbH, who sell commercial liscences for it. A related team at Cambridge University works on CompCertTSO, which extending CompCert specifically to concurrent systems.


\newpage
 
\begin{thebibliography}{9}

\bibitem{paper}
F. Besson, S. Blazy and P. Wilke \textit{CompCertS: A Memory-Aware Verified C Compiler using Pointer as Integer Semantics}
\\\texttt{http://people.rennes.inria.fr/Frederic.Besson/compcerts.pdf}

\bibitem{besson}
Frédéric Besson
\\\texttt{http://people.rennes.inria.fr/Frederic.Besson/}

\bibitem{blazy}
Sandrine Blazy
\\\texttt{http://people.rennes.inria.fr/Sandrine.Blazy/}

\bibitem{wilke}
Pierre Wilke
\\\texttt{http://www.cs.yale.edu/homes/wilke-pierre/}

\bibitem{compcert-C}
The CompCert C compiler
\\\texttt{http://compcert.inria.fr/compcert-C.html}

\bibitem{ITP}
Interactive Theorem Proving (conference)
\\\texttt{https://en.wikipedia.org/wiki/Interactive\_Theorem\_Proving\_(conference)}

\bibitem{CADE}
Conference on Automated Deduction
\\\texttt{http://www.cadeinc.org/}

\bibitem{TABLEAUX}
International Conference on Automated Reasoning with Analytic Tableaux and Related Methods 
\\\texttt{http://www.tableaux-ar.org/}
% \\\texttt{https://en.wikipedia.org/wiki/International\_Conference\_on\_Automated\_Reasoning\_with\_Analytic\_Tableaux\_and\_Related\_Methods}

\bibitem{IJCAR}
International Joint Conference on Automated Reasoning
\\\texttt{http://ijcar.org/}

\bibitem{celtique}
Project-Team CELTIQUE, Software certification with semantic analysis
\\\texttt{https://team.inria.fr/celtique/}

\bibitem{x_leroy_compcert}
X. Leroy. \textit{Formal verification of a realistic compiler}. C. ACM, 52(7):107–115, 2009

\bibitem{besson_blazy_wilke_2014}
F. Besson, S. Blazy, and P. Wilke.
\textit{A precise and abstract memory model for C using symbolic values}. In APLAS, volume 8858 of LNCS, 2014.

\bibitem{besson_blazy_wilke_2015}
F. Besson, S. Blazy, and P. Wilke.  
\textit{A concrete memory model for CompCert}.  In ITP, volume 9236 of LNCS. Springer, 2015.

\bibitem{besson_blazy_wilke_2017}
F. Besson, S. Blazy, and P. Wilke. 
\textit{A Verified CompCert Front-End for a Memory Model Supporting Pointer Arithmetic and Uninitialised Data} Journal of Automated Reasoning, 2017.

\end{thebibliography}




\bibliographystyle{plain}
%\bibliography{report}
\end{document}
